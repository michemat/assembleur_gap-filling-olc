#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import time
import os
import psutil
import re

########################################## OPTION TERMINAL #########################################
#Définit les options accessibles depuis le terminal
def terminalOptions():
    parser = argparse.ArgumentParser()
    parser.add_argument("start_stop", help ="Nom du fichier contenant les reads start et stop\
                        (premier argument)")
    parser.add_argument("reads_filename", help ="Nom du fichier contenant les reads à assembler\
                        (deuxième argument)" )
    
    parser.add_argument('-k','-kmer', type=int, default = 20, help = "Définit la taille des kmers\
                        (taille minimum de chevaucement)")
    parser.add_argument('-e','-erreur', type=int, default = 2, help = "Nombre d'erreurs par\
                        chevauchement autorisées")
    parser.add_argument('-omax', '-overlapmax', type=int, default = 100, help = "Définit la taille\
                        maximum de chevauchement")
    parser.add_argument('-omin', '-overlapmin', type=int, default = 30, help = "Définit la taille\
                        minimum de chevauchement (kmer comprit)")
    
    
    parser.add_argument('-a', '-all', '-allpath', action ="store_true", help ="Retourne tout\
                        les autres chemins possibles")
    parser.add_argument('-p', '-print', action ="store_true", help ="Affiche le premier chemin\
                        trouvé dans le terminal")
    parser.add_argument('-m', '-minsize', action ="store_true", help ="Ne sélectionne que les\
                        chemins faisant au moins la taille du chemin recherché, ")
                        
    parser.add_argument('-l','-len', type=int, default = 0, help = "Définit la taille\
                        de la séquence à retrouver")
    
    
    args = parser.parse_args()
    return args

####################################### LECTURE DES FICHIERS #######################################
#Lecture du fichier contenant les reads start et stop
#le fichier doit contenir le header de la séquence start sur la 1er ligne
#la séquence start sur la 2e ligne
#le header de la sequence stop sur la 3e ligne
#la sequence stop sur la 4e ligne

def parsingStartStopFile(start_stop, extractLenBool):
    startFile = open(start_stop, "r")
    headerStart = startFile.readline()
    start = startFile.readline()
    start = start[:len(start) - 1]
    headerEnd = startFile.readline()
    end = startFile.readline()
    end = end[:len(end) - 1]
    startFile.close()
    
    totalLength = 0
    #Extrait la distance entre le kmer start et le kmer end
    if extractLenBool :
        parInd = headerStart.find("(")
        tiretInd = headerStart.find("-")
        nbStart = headerStart[parInd + 1:tiretInd]
        parInd = headerEnd.find(")")
        tiretInd = headerEnd.find("-")
        nbEnd = headerEnd[tiretInd + 1:parInd]
        
        #totalLength correspond à la distance entre le premier nucléotide du kmer start
        #et le dernier nucléotide du kmer stop
        totalLength = int(nbEnd) - int(nbStart)
            
    return start, end, totalLength
    
    
def parsingReadFile(pathFile):
    #cherche l'extension du fichier
    pathSplit = pathFile.split("/")
    filename = pathSplit[len(pathSplit) - 1]
    filenameSplit = filename.split(".")
    extension = filenameSplit[len(filenameSplit) - 1]
    
    readFile = open(pathFile, "r")
    #Initialise le tableau pour stocker les reads
    tabseq = [] 
    #Lecture du fichier si extension .Fasta
    if extension == "fasta":
	#Cherche toutes les lignes commencant par >
	#(Correspondant aux différents reads)
        p = re.compile('>.*')
        for line in readFile:
            m = p.match(line)
            if not m:
                #on enlève le \n à la fin de la ligne
                l = line[:len(line)-1]
                tabseq.append(l)
        
    #Lecture fichier fastq
    #Si la ligne est "+" la sequence est la ligne d'avant
    elif extension == "fastq" or extension == "fq":
        lines = readFile.readlines()
        i = 0
        for i in range(len(lines)):
            if "+" in lines[i] :
                l = lines[i - 1][:len(lines[i - 1])-1]
                tabseq.append(l) 
            i +=1
    readFile.close()
    return tabseq

######################################## FONCTIONS ################################################# 
#Renvoie le reverse complement d'une séquence
def reverseRead(seq):
    return seq.replace("T","a").replace("A","t").replace("C", "g").replace("G","c").upper()[::-1]

#Test si les string seq1 et seq2 sont égales avec un nombre de différence de nberrorMaxau maximum
#renvoie -1 si il y a trop d'erreurs, renvoie le nombre d'erreurs sinon.
def is_equalStringWithError(seq1, seq2, nberrorMax):
    nberror = 0
    for i in range(len(seq1)):
        if seq2[i] != seq1[i]:
             nberror = nberror + 1
             if nberror > nberrorMax:
                 return -1
    return nberror
    
#Cherche le premier read start dans tous les reads disponibles
#Renvoie "" si aucun read trouvé
def find_start_read(tabseq, start):
    for read in tabseq:
        if(start in read):
            return read
    return ""

#Retourne S1 et S2 concaténés sans répéter la partie chevauchante de S2
#OverlapSize correspond à la taille du chevauchement entre la fin de S1 et le début de S2
def extend(s1, s2, overlapSize):
    return s1 + s2[overlapSize:]

#À partir de l'indice d'une sequence dans tab renvoie sa valeur
#Si "-" devant l'indice, renvoie le reverse complement de la séquence
def indiceToSeq(i, tab):
    if i[0] == "-":
        ind = int(i[1:])
        seq = reverseRead(tab[abs(ind)])
    else: 
        ind = int(i)
        seq = tab[ind]  
    return seq 

####################################################################################################
#Remplis le dictionnaire dic avec tout les préfixes (de longeur k) de tout les reads
#Où la clé est le prefixe du read et la valeur est la liste des indices des reads dans tab
#Stocke églement les reverses des reads, Pour cela on ajoute un "-" devant les indices des reads 
#(les indices sont stockés sous forme de string)
def fillDictionary(tab, k):
    dic = {}
    i = 0
    for read in tab:    
        pref = read[:k]
        prefinv = reverseRead(pref)
        if pref not in dic.keys(): 
            dic[pref] = []
        dic[pref].append(str(i))
        
        if prefinv not in dic.keys(): 
            dic[prefinv] = []
        dic[prefinv].append("-" + str(i))
        i = i + 1
    return dic
                
#################################### ALGORITHME PRINCIPAL ##########################################
#Retourne le 1er chemin trouvé
#En autorisant un nombre d'erreurs par chevauchement nbErrorPerOverlaps 
def assembling(seqtot, nbErrorPerOverlaps, prefixDic, end, minSize, totalLength, k, overlapMax,\
               overlapMin, tabseq):
    #Si on trouve le read stop dans la séquence en cours et si on a atteint un minimum de taille 
    #(lorsque l'option est active)
    if(end in seqtot and len(seqtot) >= minSize):
        return seqtot, True
    
    #Si la sequence en cours est plus grande que la taille de la séquence à trouver
    #Stop l'execution et, renvoie faux
    if len(seqtot) > totalLength:
        return "", False
    
    #seqref = fin de la sequence en cours sur laquelle on va chercher un chevauchement
    seqref = seqtot[-overlapMax:]
    
    #On parcours kmer par kmer seqref
    #Pour chaque kmer on regarde les reads qui commence par ce kmer dans le dictionnaire prefixDic
    #Pour tout ces reads on test le chevauchement entre la fin de la séquence et le read 
    #en tolérant nbErrorPerOverlaps
    #Si le chevauchement est suffisant on étend la séquence en cours avec le read
    #On réappelle alors la fonction sur cette nouvelle séquence
	
    #Pour chaque kmer de seqtot  
    for i in range (len(seqref) - k + 1):
        kmer = seqref[i:i+k]
        #Si le kmer est dans le dictionnaire        
        if kmer in prefixDic.keys():   
            #On regarde tout les reads associé au kmer dans le dictionnaire
            for indread in prefixDic[kmer]:
                read = indiceToSeq(indread, tabseq)    
                #Si le chevauchement est suffisant avec une longeur également suffisante
                if (is_equalStringWithError(seqref[i + k:], read[k:overlapMax - i], \
                                nbErrorPerOverlaps) != -1 and len(seqref[i:]) > overlapMin):
                    
                    #On retire le kmer dictionnaire pour éviter une boucle infinie
                    prefixDic[kmer].remove(indread)
                    #Si on a utilisé tout les reads de préfixe kmer on supprime le kmer du 
                    #dictionnaire de préfixe
                    if prefixDic[kmer] == []:
                        del prefixDic[kmer]

                    #On étend la séquence
                    seqtot = extend(seqtot, read, len(seqref[i:]))
                    #Appel récursif
                    res, succes = assembling(seqtot, nbErrorPerOverlaps, prefixDic, end, minSize, \
                                             totalLength, k, overlapMax, overlapMin, tabseq)
                    if succes :
                        return res, True
    return "", False

##################################### TOUS LES CHEMINS (bonus) #####################################
#Pour renvoyer tout les chemins on reprend le même algorithme, en enlevant la fin d'execution 
#lorsque l'on trouve un chemin 
#et en stockant tout les chemins dans une variable paths
#On utilise une fonction auxiliaire afin de ne pas stocker paths comme une variable globale

def assemblingAllPath(seqtot, nbErrorPerOverlaps, prefixDic, end, minSize, totalLength, k,\
                      overlapMax, overlapMin, tabseq):
    paths = []
    #Utilisation d'une fonction auxiliaire afin que paths ne soit pas une variable globale
    def aux(seqtot, prefixDic):
        if(end in seqtot and len(seqtot) >= minSize):
            paths.append(seqtot)
            return
        
        if len(seqtot) > totalLength:
             return "", False
    
        seqref = seqtot[-overlapMax:]
        
        for i in range(len(seqref) - k + 1):     
            kmer = seqref[i:i+k]
            #test si kmer est dans dico         
            if kmer in prefixDic.keys():            
                for indread in prefixDic[kmer]:
                    read = indiceToSeq(indread, tabseq)                
                    if (is_equalStringWithError(seqref[i + k:], read[k:overlapMax - i], \
                                nbErrorPerOverlaps) != -1 and len(seqref[i:]) > overlapMin):
                        prefixDic[kmer].remove(indread)
                        if prefixDic[kmer] == []:
                            del prefixDic[kmer]                         
                        seqtot = extend(seqtot, read, len(seqref[i:]))
                        aux(seqtot, prefixDic)

    
    aux(seqtot, prefixDic)
    return paths

#################################### EXECUTION ET SORTIE ###########################################
    
def main():
    #Définit des variables via des options terminals
    args = terminalOptions()    
    start_stop = args.start_stop
    pathFile = args.reads_filename
    
    k = args.k
    overlapMax = args.omax
    nbErrorPerOverlapsGlobal = args.e
    overlapMin = args.omin
    overlapMax = args.omax
    minSizeBool = args.m   
    lenseq = args.l
        
    if overlapMin > k :
        overlapMin = k
    if overlapMin > overlapMax :
        print("paramètre d'overlap non cohérent")
        exit()
   
    #Parsing
    start_time = time.time()
    if lenseq == 0 :
        start, end, totalLength = parsingStartStopFile(start_stop, True)
    else :
        start, end, totalLength = parsingStartStopFile(start_stop, False)
        totalLength = lenseq
        
    #On rajoute arbitrairement 20% à la taille de totalLength pour ne pas être trop stringent
    totalLength = totalLength + totalLength * 20/100
    tabseq = parsingReadFile(pathFile)
    readStart = find_start_read(tabseq, start)
    
    #Définis la taille minimale de la séquence si demandé en paramètre
    min_size = 0 
    if minSizeBool:
        min_size = totalLength
    
    if tabseq == []:
        print("Extension du fichier contenant les reads non reconnue")
    if readStart == "":
        print("Pas de read start trouvé")
    else:
        prefDic = fillDictionary(tabseq, k)
        print("Parsing effectué en : %.4f secondes" %(time.time() - start_time))
        start_time = time.time()
        result = assembling(readStart, nbErrorPerOverlapsGlobal, prefDic.copy(), end, min_size, \
                            totalLength, k, overlapMax, overlapMin, tabseq)
        end_time = time.time() - start_time
        if not result[1] :
            print("Pas de séquence trouvé, essayé avec une valeur de k plus petite ou un nombre \
                  d'erreurs par chevauchement plus grand")
        else:
            print("Premier chemin trouvé en : %.4f secondes" %(end_time))
            if args.p :
                print("Séquence retrouvé :")
                print(result[0])
            
            content = "Premier chemin trouvé :\n"
            content += result[0]
            
            if args.a :
                start_time = time.time()
                paths = assemblingAllPath(readStart, nbErrorPerOverlapsGlobal, prefDic.copy(), \
                                    end, min_size, totalLength, k, overlapMax, overlapMin, tabseq)
    
                end_time = time.time() - start_time
                
                if result in paths:
                    paths.remove(result)
                    
                print("%d Autres chemins trouvés en : %.4f secondes" %(len(paths), end_time))
                content += "\n\nAutres chemins trouvés :\n"
                n = 0
                for path in paths:
                    content += "["+ str(n) +"]" + path + "\n"
                    n += 1
            f = open("resultat.txt", "w+")
            f.write(content)
            f.close
            	    
            process = psutil.Process(os.getpid())
            print("Mémoire utilisé par le processus : %d bytes" %(process.memory_info().rss))
            
if __name__ =='__main__' :
    main()