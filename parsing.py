import re

####################################### LECTURE DES FICHIERS #######################################
#Lecture du fichier contenant les reads start et stop
#le fichier doit contenir le header de la séquence start sur la 1er ligne
#la séquence start sur la 2e ligne
#le header de la sequence stop sur la 3e ligne
#la sequence stop sur la 4e ligne

def parsingStartStopFile(start_stop, extractLenBool):
    startFile = open(start_stop, "r")
    headerStart = startFile.readline()
    start = startFile.readline()
    start = start[:len(start) - 1]
    headerEnd = startFile.readline()
    end = startFile.readline()
    end = end[:len(end) - 1]
    startFile.close()
    
    totalLength = 0
    #Extrait la distance entre le kmer start et le kmer end
    if extractLenBool :
        parInd = headerStart.find("(")
        tiretInd = headerStart.find("-")
        nbStart = headerStart[parInd + 1:tiretInd]
        parInd = headerEnd.find(")")
        tiretInd = headerEnd.find("-")
        nbEnd = headerEnd[tiretInd + 1:parInd]
        
        #totalLength correspond à la distance entre le premier nucléotide du kmer start
        #et le dernier nucléotide du kmer stop
        totalLength = int(nbEnd) - int(nbStart)
            
    return start, end, totalLength
    
    
def parsingReadFile(pathFile):
    #cherche l'extension du fichier
    pathSplit = pathFile.split("/")
    filename = pathSplit[len(pathSplit) - 1]
    filenameSplit = filename.split(".")
    extension = filenameSplit[len(filenameSplit) - 1]
    
    readFile = open(pathFile, "r")
    #Initialise le tableau pour stocker les reads
    tabseq = [] 
    #Lecture du fichier si extension .Fasta
    if extension == "fasta":
	#Cherche toutes les lignes commencant par >
	#(Correspondant aux différents reads)
        p = re.compile('>.*')
        for line in readFile:
            m = p.match(line)
            if not m:
                #on enlève le \n à la fin de la ligne
                l = line[:len(line)-1]
                tabseq.append(l)
        
    #Lecture fichier fastq
    #Si la ligne est "+" la sequence est la ligne d'avant
    elif extension == "fastq" or extension == "fq":
        lines = readFile.readlines()
        i = 0
        for i in range(len(lines)):
            if "+" in lines[i] :
                l = lines[i - 1][:len(lines[i - 1])-1]
                tabseq.append(l) 
            i +=1
    readFile.close()
    return tabseq