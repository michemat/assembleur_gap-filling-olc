Ce programme est un assembleur OLC qui permet de faire du “Gap Filling”. Il construit une
séquence entre deux kmers donnés (start et stop) à partir d’un jeu de reads de type Illumina.
Selon les options choisies, il permet de reconstruire seulement une séquence ou bien toutes les
séquences possibles. <br>


Execute : **python assembling.py start_stop.fa reads.fasta** <br>


Exemple de données : http://bioinformatique.rennes.inria.fr/ALG2019/data/ecoli/
