######################################## FONCTIONS ################################################# 
#Renvoie le reverse complement d'une séquence
def reverseRead(seq):
    return seq.replace("T","a").replace("A","t").replace("C", "g").replace("G","c").upper()[::-1]

#Test si les string seq1 et seq2 sont égales avec un nombre de différence de nberrorMaxau maximum
#renvoie -1 si il y a trop d'erreurs, renvoie le nombre d'erreurs sinon.
def is_equalStringWithError(seq1, seq2, nberrorMax):
    nberror = 0
    for i in range(len(seq1)):
        if seq2[i] != seq1[i]:
             nberror = nberror + 1
             if nberror > nberrorMax:
                 return -1
    return nberror
    
#Cherche le premier read start dans tous les reads disponibles
#Renvoie "" si aucun read trouvé
def find_start_read(tabseq, start):
    for read in tabseq:
        if(start in read):
            return read
    return ""

#Retourne S1 et S2 concaténés sans répéter la partie chevauchante de S2
#OverlapSize correspond à la taille du chevauchement entre la fin de S1 et le début de S2
def extend(s1, s2, overlapSize):
    return s1 + s2[overlapSize:]

#À partir de l'indice d'une sequence dans tab renvoie sa valeur
#Si "-" devant l'indice, renvoie le reverse complement de la séquence
def indiceToSeq(i, tab):
    if i[0] == "-":
        ind = int(i[1:])
        seq = reverseRead(tab[abs(ind)])
    else: 
        ind = int(i)
        seq = tab[ind]  
    return seq 

####################################################################################################
#Remplis le dictionnaire dic avec tout les préfixes (de longeur k) de tout les reads
#Où la clé est le prefixe du read et la valeur est la liste des indices des reads dans tab
#Stocke églement les reverses des reads, Pour cela on ajoute un "-" devant les indices des reads 
#(les indices sont stockés sous forme de string)
def fillDictionary(tab, k):
    dic = {}
    i = 0
    for read in tab:    
        pref = read[:k]
        prefinv = reverseRead(pref)
        if pref not in dic.keys(): 
            dic[pref] = []
        dic[pref].append(str(i))
        
        if prefinv not in dic.keys(): 
            dic[prefinv] = []
        dic[prefinv].append("-" + str(i))
        i = i + 1
    return dic